﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EcoPay.Models;
using Microsoft.EntityFrameworkCore;

namespace EcoPay.Service.Context
{
    public class EcoPayContext : DbContext
    {
        public EcoPayContext(DbContextOptions<EcoPayContext> options) : base(options)
        {
            
        }

        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Notification>().ToTable("Notifications");
            modelBuilder.Entity<Transaction>().ToTable("Transactions");
        }


    }
}
