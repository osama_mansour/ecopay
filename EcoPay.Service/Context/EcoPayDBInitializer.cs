﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EcoPay.Models;

namespace EcoPay.Service.Context
{
    public class EcoPayDBInitializer
    {
        private EcoPayContext _context;

        public EcoPayDBInitializer(EcoPayContext context)
        {
            _context = context;
        }

        public async Task Seed()
        {

            var notificationList = new List<Notification>()
            {
                new Notification() { Title="Test Notification 1", StartDate=DateTime.Now}
            };

            var transactionList = new List<Transaction>()
            {
                new Transaction() {Date = DateTime.Now, Amount = 10.95, Description = "Test Transaction"}
            };

            if (!_context.Notifications.Any())
            {
                foreach (var notification in notificationList)
                {
                    _context.Notifications.Add(notification);
                }
            }

            var test = _context.Transactions.Any();

            if (!_context.Transactions.Any())
            {
                foreach (var transaction in transactionList)
                {
                    _context.Transactions.Add(transaction);
                }
            }

            _context.SaveChanges();
        }
    }
}
