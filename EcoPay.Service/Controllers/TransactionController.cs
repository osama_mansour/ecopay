﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EcoPay.Models;
using EcoPay.Service.Context;
using EcoPay.Service.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EcoPay.Service.Controllers
{
    [Produces("application/json")]
    [Route("api/Transaction")]
    public class TransactionController : Controller
    {
        private TransactionService transactionService;


        public TransactionController(EcoPayContext context)
        {
            transactionService = new TransactionService(context);
        }

        // GET: api/Transaction
        [HttpGet]
        [Route("GetAllTransactionsList")]
        public IActionResult Get()
        {
            var response = transactionService.GetTransactionList();

            return Ok(response);
        }

        //// GET: api/Transaction/5
        //[HttpGet("{id}", Name = "Get")]
        //public string Get(int id)
        //{
        //    return "value";
        //}
        
        //// POST: api/Transaction
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        [HttpPost]
        [Route("Uploader")]
        public IActionResult Uploader(List<Transaction> transactions)
        {
            return Ok(transactions.Count);
        }
        
        //// PUT: api/Transaction/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}
        
        //// DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
