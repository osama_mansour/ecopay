﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EcoPay.Models;
using EcoPay.Service.Context;
using EcoPay.Service.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EcoPay.Service.Controllers
{
    [Produces("application/json")]
    [Route("api/Notification")]
    public class NotificationController : Controller
    {
        private NotificationService notificationService;


        public NotificationController(EcoPayContext context)
        {
            notificationService = new NotificationService(context);
        }

        // GET: api/Notification/5
        [HttpGet]
        [Route("GetNotification")]
        public IActionResult Get(int id)
        {
            var response = notificationService.GetNotification(id);

            return Ok(response);
        }

        // GET: api/Notification
        [HttpGet]
        [Route("GetNotificationList")]
        public IActionResult GetNotificationList()
        {
            var response = notificationService.GetNotificationList();

            return Ok(response);
        }

        // POST: api/Notification
        [HttpPost]
        [Route("AddNotification")]
        public void Post(Notification notification)
        {
            notificationService.AddNotification(notification);
        }

        // PUT: api/Notification/5
        [HttpPut("{id}")]
        public void Put(int id, Notification notification)
        {
            notificationService.UpdateNotification(id, notification);
        }

        //DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            notificationService.DeleteNotification(id);
        }
    }
}
