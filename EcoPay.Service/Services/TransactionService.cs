﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EcoPay.Models;
using EcoPay.Service.Context;
using EcoPay.Service.Repositories;

namespace EcoPay.Service.Services
{
    public class TransactionService
    {
        private TransactionRepository _TransactionRepository;


        public TransactionService(EcoPayContext context)
        {
            _TransactionRepository = new TransactionRepository(context);
        }


        public int AddTransaction(Transaction transaction)
        {
            return _TransactionRepository.AddTransaction(transaction);
        }

        public int UpdateTransaction(int id, Transaction transaction)
        {
            return _TransactionRepository.UpdateTransaction(id, transaction);
        }

        public Transaction GetTransaction(int id)
        {
            return _TransactionRepository.GetTransaction(id);
        }

        public List<Transaction> GetTransactionList()
        {
            return _TransactionRepository.GetTransactionList().ToList();
        }

        public int DeleteTransaction(int id)
        {
            return _TransactionRepository.DeleteTransaction(id);
        }
    }
}
