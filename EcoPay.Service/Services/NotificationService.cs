﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EcoPay.Service.Context;
using EcoPay.Models;
using EcoPay.Service.Repositories;

namespace EcoPay.Service.Services
{
    public class NotificationService
    {
        private NotificationRepository _NotificationRepository;


        public NotificationService(EcoPayContext context)
        {
            _NotificationRepository = new NotificationRepository(context);
        }


        public int AddNotification(Notification notification)
        {
            return _NotificationRepository.AddNotification(notification);
        }

        public int UpdateNotification(int id, Notification notification)
        {
            return _NotificationRepository.UpdateNotification(id, notification);
        }

        public Notification GetNotification(int id)
        {
            return _NotificationRepository.GetNotification(id);
        }

        public List<Notification> GetNotificationList()
        {
            return _NotificationRepository.GetNotificationList().ToList();
        }

        public int DeleteNotification(int id)
        {
            return _NotificationRepository.DeleteNotification(id);
        }
    }
}
