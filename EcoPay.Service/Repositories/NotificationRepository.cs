﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using EcoPay.Service.Context;
using EcoPay.Models;

namespace EcoPay.Service.Repositories
{
    public class NotificationRepository
    {
        private EcoPayContext _context;
        public NotificationRepository(EcoPayContext context)
        {
            _context = context;
        }

        public int AddNotification(Notification n)
        {
            _context.Notifications.Add(n);
            int response = _context.SaveChanges();
            return response;
        }

        public int DeleteNotification(int id)
        {
            int response = 0;
            var notificaiton = _context.Notifications.FirstOrDefault(n => n.ID == id);
            if (notificaiton != null)
            {
                _context.Notifications.Remove(notificaiton);
                response = _context.SaveChanges();
            }

            return response;
        }

        public Notification GetNotification(int id)
        {
            var notification = _context.Notifications.FirstOrDefault(b => b.ID == id);
            return notification;
        }

        public IEnumerable<Notification> GetNotificationList()
        {
            var notification = _context.Notifications.ToList();
            return notification;
        }

        public int UpdateNotification(int id, Notification n)
        {
            var response = 0;
            var notification = _context.Notifications.Find(id);
            if (notification != null)
            {
                notification.Title = n.Title;
                notification.StartDate = n.StartDate;

                response = _context.SaveChanges();
            }

            return response;
        }
        
    }
}
