﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EcoPay.Models;
using EcoPay.Service.Context;

namespace EcoPay.Service.Repositories
{
    public class TransactionRepository
    {
        private EcoPayContext _context;
        public TransactionRepository(EcoPayContext context)
        {
            _context = context;
        }

        public int AddTransaction(Transaction t)
        {
            _context.Transactions.Add(t);
            int response = _context.SaveChanges();
            return response;
        }

        public int DeleteTransaction(int id)
        {
            int response = 0;
            var notificaiton = _context.Transactions.FirstOrDefault(n => n.ID == id);
            if (notificaiton != null)
            {
                _context.Transactions.Remove(notificaiton);
                response = _context.SaveChanges();
            }

            return response;
        }

        public Transaction GetTransaction(int id)
        {
            var notification = _context.Transactions.FirstOrDefault(b => b.ID == id);
            return notification;
        }

        public IEnumerable<Transaction> GetTransactionList()
        {
            var notification = _context.Transactions.ToList();
            return notification;
        }

        public int UpdateTransaction(int id, Transaction t)
        {
            var response = 0;
            var transaction = _context.Transactions.Find(id);
            if (transaction != null)
            {
                transaction.Amount = t.Amount;
                transaction.Date = t.Date;
                transaction.Description = t.Description;

                response = _context.SaveChanges();
            }

            return response;
        }
    }
}
