﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EcoPay.Models
{
    public class Notification
    {
        [Key]
        public int ID { get; set; }
        public string Title { get; set; }
        public DateTime StartDate { get; set; }
    }
}
