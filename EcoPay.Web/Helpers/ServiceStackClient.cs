﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ServiceStack;

namespace EcoPay.Web.Helpers
{
    public class ServiceStackClient
    {
        public static JsonServiceClient GetClient(string endpointUri)
        {
            return new JsonServiceClient(endpointUri);
        }
    }
}
