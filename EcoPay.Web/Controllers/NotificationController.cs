﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EcoPay.Models;
using EcoPay.Web.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ServiceStack;

namespace EcoPay.Web.Controllers
{
    public class NotificationController : Controller
    {
        private IConfiguration _configuration;
        private JsonServiceClient ecoPayService;
        public NotificationController(IConfiguration configuration)
        {
            _configuration = configuration;

            var config = _configuration["AppSettings:EcoPayService"];
            ecoPayService = ServiceStackClient.GetClient(config);

        }

        // GET: Notification
        public ActionResult Index()
        {
            var result = ecoPayService.Get<List<Notification>>("/Notification/GetNotificationList");

            return View(result);
        }

        
        
    }
}