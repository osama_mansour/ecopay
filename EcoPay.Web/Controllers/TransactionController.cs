﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using EcoPay.Models;
using EcoPay.Web.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ServiceStack;

namespace EcoPay.Web.Controllers
{
    public class TransactionController : Controller
    {
        private IConfiguration _configuration;
        private JsonServiceClient ecoPayService;
        public TransactionController(IConfiguration configuration)
        {
            _configuration = configuration;

            var config = _configuration["AppSettings:EcoPayService"];
            ecoPayService = ServiceStackClient.GetClient(config);

        }


        public IActionResult Index()
        {
            var result = ecoPayService.Get<List<Transaction>>("/Transaction/GetAllTransactionsList");

            return View(result);
        }

        [HttpPost]
        public async Task<IActionResult> Upload(IFormFile files)
        {

            List<Transaction> Transactions = new List<Transaction>();

            using (var stream = files.OpenReadStream())
            {
                using (var reader = new StreamReader(stream))
                {

                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var data = line.Split(",");

                        var date = Convert.ToDateTime(data[0]);
                        var amount = Convert.ToDouble(data[1]);
                        var description = data[2];

                        var trans = new Transaction() { Date = date, Amount = amount, Description = description };

                        Transactions.Add(trans);
                    }
                }
            }

            var response = ecoPayService.Post<string>("/Transaction/Uploader", Transactions);


            return Ok(response);

        }
    }
}