﻿using EcoPay.Models;
using System;
using System.Collections.Generic;

//Can Return any generic Objects Reponse
namespace EcoPay.ServiceModels
{
    public class NotificationResponse
    {
        public Notification Item { get; set; }
        public string Response { get; set; }
    }

    public class NotificationListResponse
    {
        public List<Notification> Items { get; set; }
        public string Response { get; set; }
    }
}
