﻿using ServiceStack;
//Can create a request for any object
namespace EcoPay.ServiceModels
{
    public class NotificationRequest : IReturn<NotificationResponse>
    {
        public int Id { get; set; }
    }
}
