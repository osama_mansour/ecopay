﻿using System;
//Can Return any generic Objects Reponse
namespace EcoPay.ServiceModels
{
    public class NotificationResponse<T>
    {
        public T Result { get; set; }

        public string responseMessage { get; set; }
    }
}
