﻿using ServiceStack;
//Can create a request for any object
namespace EcoPay.ServiceModels
{
    public class NotificationRequest<T> : IReturn<NotificationResponse<T>>
    {
        public T Object { get; set; }
    }
}
